document.addEventListener('DOMContentLoaded', () => {
    const taskForm = document.getElementById('task-form');
    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');

    // Load tasks from local storage
    const tasks = JSON.parse(localStorage.getItem('tasks')) || [];
    tasks.forEach(task => addTaskToList(task));

    taskForm.addEventListener('submit', (e) => {
        e.preventDefault();
        const task = taskInput.value;
        addTaskToList(task);
        tasks.push(task);
        localStorage.setItem('tasks', JSON.stringify(tasks));
        taskInput.value = '';
    });

    function addTaskToList(task) {
        const li = document.createElement('li');
        li.textContent = task;
        li.classList.add('bg-white', 'p-3', 'rounded-lg', 'flex', 'justify-between', 'items-center');
        const deleteBtn = document.createElement('button');
        deleteBtn.innerHTML = '<i class="fas fa-trash-alt"></i>';
        deleteBtn.classList.add('text-red-500');
        deleteBtn.addEventListener('click', () => {
            taskList.removeChild(li);
            const index = tasks.indexOf(task);
            tasks.splice(index, 1);
            localStorage.setItem('tasks', JSON.stringify(tasks));
        });
        li.appendChild(deleteBtn);
        taskList.appendChild(li);
    }
});
